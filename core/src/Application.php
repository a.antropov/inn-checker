<?php declare(strict_types=1);

namespace App;

use App\Inn\Checker;

class Application
{
    /**
     * @var Checker|null
     */
    private static $innChecker;

    /**
     * @return Checker
     */
    public static function InnChecker(): Checker
    {
        if (self::$innChecker === null) {
            self::$innChecker = new Checker();
        }

        return self::$innChecker;
    }
}
