<?php declare(strict_types=1);

namespace App\Inn;

use GuzzleHttp\Client;

class Checker
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function check(string $inn)
    {
        $this->client->get('https://pb.nalog.ru/search-proc.json');
    }
}
